import argparse
from typing import TextIO
from enum import Enum, auto


class Transform(Enum):
    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()
    UP_LEFT = auto()
    UP_RIGHT = auto()
    DOWN_LEFT = auto()
    DOWN_RIGHT = auto()


ALL_TRANSFORMS = [Transform.UP, Transform.DOWN, Transform.LEFT, Transform.RIGHT, Transform.UP_LEFT, Transform.UP_RIGHT,
                  Transform.DOWN_LEFT, Transform.DOWN_RIGHT]

TRANSFORM_MAP = {
    Transform.UP: (-1, 0),
    Transform.DOWN: (1, 0),
    Transform.LEFT: (0, -1),
    Transform.RIGHT: (0, 1),
    Transform.UP_LEFT: (-1, -1),
    Transform.UP_RIGHT: (-1, 1),
    Transform.DOWN_LEFT: (1, -1),
    Transform.DOWN_RIGHT: (1, 1),
}


class Octopus:
    def __init__(self, energy):
        self.energy = energy
        self.flashed = False

    def add_energy(self, energy):
        if not self.flashed:
            self.energy += energy

    def try_flash(self):
        if not self.flashed:
            if self.energy > 9:
                self.flashed = True
                self.energy = 0
                return True
        return False

    def has_flashed(self):
        return self.flashed

    def reset_flash(self):
        self.flashed = False


class OctopusGrid:
    def __init__(self, text):
        self.grid = []
        for line in text.split('\n'):
            octo_line = []
            for energy in line:
                octo_line.append(Octopus(int(energy)))
            self.grid.append(octo_line)
        self.num_rows = len(self.grid)
        self.num_cols = len(self.grid[0])

    def transform(self, position, transform):
        row, column = position
        offset_row, offset_column = TRANSFORM_MAP[transform]
        return row + offset_row, column + offset_column

    def is_position_legal(self, position):
        row, column = position
        return 0 <= row < self.num_rows and 0 <= column < self.num_cols

    def get_legal_transforms(self, position):
        transforms = [self.transform(position, t) for t in ALL_TRANSFORMS]
        transforms = [t for t in transforms if self.is_position_legal(t)]
        return transforms

    def handle_flash(self, position):
        row, column = position
        octopus = self.grid[row][column]
        flashed = octopus.try_flash()
        if flashed:
            neighbors = self.get_legal_transforms(position)
            for neighbor_position in neighbors:
                n_row, n_col = neighbor_position
                self.grid[n_row][n_col].add_energy(1)
                self.handle_flash(neighbor_position)

    def step(self):
        for row in self.grid:
            for octopus in row:
                octopus.add_energy(1)

        for row in range(self.num_rows):
            for column in range(self.num_cols):
                self.handle_flash((row, column))

        flashes = 0
        for row in self.grid:
            for octopus in row:
                flashes += 1 if octopus.has_flashed() else 0
                octopus.reset_flash()

        return flashes


def part_1(input_file: TextIO):
    grid = OctopusGrid(input_file.read())
    flashes = 0
    for step in range(100):
        flashes += grid.step()
    return flashes


def part_2(input_file: TextIO):
    grid = OctopusGrid(input_file.read())
    flashes = 0
    step = 0
    while flashes != 100:
        flashes = grid.step()
        step += 1
    return step


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 11')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
